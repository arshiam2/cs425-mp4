#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import print_function

import sys
from operator import add

import random

from pyspark.sql import SparkSession

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: filter <file>", file=sys.stderr)
        sys.exit(-1)

    spark = SparkSession.builder.appName("PythonJoin").getOrCreate() 

    lines = spark.read.text(sys.argv[1]).rdd.map(lambda r: r[0])
    lines = lines.map(lambda a: a.encode('utf-8'))

    names = lines.filter(lambda x: x.split(':')[1] if 'profileName' in x else '')
    profiles = names.map(lambda c: (c, random.randint(1, 10)))

    #randos = randos.map(lambda d: (d, random.randint(1, 10))
    #final = profiles.join(randos)

    res = profiles.collect()
    
    for r in res:
        print(r)
        print('\n')

    spark.stop()
