#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import print_function

import sys
from operator import add

from pyspark.sql import SparkSession
from pyspark import SparkContext

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: wordcount <file>", file=sys.stderr)
        sys.exit(-1)

    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()
    lines = spark.read.format("csv").load(sys.argv[1]).rdd.map(lambda r: r[3])
    
    lines = lines.flatMap(lambda x: '' if x is None else x.split(' '))
    lines = lines.map(lambda a: a.encode('utf-8'))
    lines = lines.map(lambda b: (b, 1)).reduceByKey(add)
    output = lines.collect()
    for (word, count) in output:
        print("[%s: %i]" % (word, count))

    spark.stop()
