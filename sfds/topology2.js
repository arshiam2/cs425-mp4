function map_filter_by_score(string) {
  if (Number(string.score) > 3) {
    return [{ action: "END", state: string.movie }];
  }
  return [];
}

function resolver(data) {
  switch (data.action) {
    case "START":
      return map_filter_by_score(data.state);
    default:
      return { action: data.action, state: "NONE" };
  }
}

module.exports = resolver;
