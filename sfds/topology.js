function map_add_hi(string) {
  let new_states = [];
  let words = string.split(" ").forEach(word => {
    new_states.push({ action: "LOWER", state: word });
  });
  return new_states;
}


function test() {
  console.log("YOOOO")
}

function map_lower(string) {
  return [{ action: "REDUCE_BY_KEY", state: string.toLowerCase() , aggregator: "count"}];
}

function resolver(data) {
  switch (data.action) {
    case "START":
      return map_add_hi(data.state);
    case "LOWER":
      return map_lower(data.state);
    default:
      return { action: data.action, state: "NONE" };
  }
}

module.exports = resolver;
