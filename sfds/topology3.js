const fs = require("fs");

let digits = [];
var readStream = fs.createReadStream("randomNumbers.txt", "utf8");
readStream
  .on("data", function(chunk) {
    digits = [...digits, ...chunk.split("\n")];
  })
  .on("end", function() {
    console.log("done");
  });

function map_add_hi(string) {
  let name = string[0];
  const data = digits.map(digit => {
    return { action: "END", state: [name, digit] };
  });
  return data;
}

function resolver(data) {
  switch (data.action) {
    case "START":
      return map_add_hi(data.state);
    default:
      return { action: data.action, state: "NONE" };
  }
}

module.exports = resolver;
