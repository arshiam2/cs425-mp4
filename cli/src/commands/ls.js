// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");
let fetch = require("node-fetch");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "ls [sdfsfilename]";
module.exports.describe =
  "Lists all the VMs that currently store sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  var target = "http://" + master + ":3000/ls/" + argv.sdfsfilename;
  request.post(target, function(error, response, body) {
    console.log('The file "' + argv.sdfsfilename + '" is on VMs ' + body);
    process.exit(0);
  });
});
