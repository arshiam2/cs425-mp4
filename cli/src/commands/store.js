// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "store [vmNumber]";
module.exports.describe = " List all SDFS files stored at specified VM.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  var target = "http://" + master + ":3000/store/" + argv.vmNumber;
  console.log(target)
  request.post(target, function(error, response, body) {
        console.log("\nThe files on VM  "+ argv.vmNumber + " include " + body);
        process.exit(0)
    })
    
});
