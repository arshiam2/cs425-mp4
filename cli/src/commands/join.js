// @flow
const path = require("path");
const chalk = require("chalk");
const process = require("process");
var request = require("request");
let fetch = require("node-fetch");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "join [number]";
module.exports.describe = "dds the specified VM to the group.";

module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  var target = "http://" + master + ":3000/join/" + argv.number;
  request.post(target, function(error, response, body) {
    process.exit(0);
  });

});
