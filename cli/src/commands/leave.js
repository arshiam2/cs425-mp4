// @flow
const path = require("path");
const chalk = require("chalk");
const process = require("process");
const request = require("request");
const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "leave [number]";
module.exports.describe = "Voluntarily removal of the specified VM from group.";

module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  var target = "http://" + master + ":3000/leave/" + argv.number;
  request.post(target, function(error, response, body) {
    process.exit(0);
  });
});
