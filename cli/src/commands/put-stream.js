// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");
var csv = require("csv-stream");
const fetch = require("node-fetch");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "put-stream [name]";
module.exports.describe = "Writes localfilename to the SDFS as sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;

queue = [];

function parseTwitterTweets(data) {
  let parsed =
    data[
      "#image_id,unixtime,rawtime,title,total_votes,reddit_id,number_of_upvotes,subreddit,number_of_downvotes,localtime,score,number_of_comments,username"
    ];
  return parsed.split(",")[3];
}

async function getCSVdata(source) {
  if (source == "reddit") {
    let allMessages = [];
    let messages = [];

    var options = {
      delimiter: "\t"
    };

    fs.createReadStream(
      "/Users/ariamalkani/distributed/storm/redditSubmissions.csv"
    )
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        let message = parseTwitterTweets(data);
        messages.push(message);
        if (messages.length == 1) {
          allMessages.push(messages);
          const body = { action: "START", state: messages[0] };
          queue.push(body);
          messages = [];
        }
      })
      .on("column", function(key, value) {})
      .on("end", function() {})
      .on("close", function() {});
  }
  if (source == "movies") {
    let allMessages = [];
    let messages = [];

    var options = {
      delimiter: "\t"
    };

    let movie = "";
    fs.createReadStream(
      "/Users/ariamalkani/distributed/test/apache-spark/python/movies.txt"
    )
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        if (
          data["product/productId: B003AI2VGA"].startsWith("product/productId:")
        ) {
          movie = data["product/productId: B003AI2VGA"];
        }
        if (data["product/productId: B003AI2VGA"].startsWith("review/score:")) {
          const body = {
            action: "START",
            state: {
              movie: movie.replace("review/summary:", ""),
              score: data["product/productId: B003AI2VGA"].replace(
                "review/score:",
                ""
              )
            }
          };
          // console.log(body);
          queue.push(body);
        }
      })
      .on("column", function(key, value) {})
      .on("end", function() {
        // if (data["product/productId: B001E4KFG0"].startsWith("review/profileName:")) {
        //   console.log(data["product/productId: B001E4KFG0"].startsWith("review/profileName:"))
        // }
        // if (data["product/productId: B001E4KFG0"].startsWith("review/score:")) {
        //   console.log(data["product/productId: B001E4KFG0"].startsWith("review/score:"))
        // }
        // if (data["product/productId: B001E4KFG0"].startsWith("review/time:")) {
        //   console.log(data["product/productId: B001E4KFG0"].startsWith("review/time:"))
        // }
        // if (messages.length > 0) {
        //   allMessages.push(messages);
        // }
      })
      .on("close", function() {});
  }

  if (source == "food") {
    // console.log("PIZXA");
    let profileName = "";
    fs.createReadStream("/Users/ariamalkani/Downloads/finefoods.txt")
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        data = Object.values(data)[0];
        if (data.startsWith("review/profileName:")) {
          profileName = data.replace("review/profileName:", "");
        }

        if (data.startsWith("review/score:")) {
          const body = {
            action: "START",
            state: [profileName, data.replace("review/score:", "")]
          };
          queue.push(body);
          // console.log(body)
          // console.log(queue.length)
          // queue = [body];
          // return
        }

        // if (data.startsWith("review/time:")) {
        //   const body = { action: "START", state: [profileName, data.replace("review/time:", "")] };
        //   queue.push(body);
        // }
      });
  }
}

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  console.log("Starting to stream data to spout");
  await getCSVdata(argv.name);
  setInterval(function sync() {
    if (queue.length > 0) {
      const body = queue.pop();
      var target = "http://" + master + ":3000/spout";
      fetch(target, {
        method: "post",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" }
      });
    } else {
      console.log("Finished streamming data to the spout");
      process.exit();
    }
  }, 25);
});
